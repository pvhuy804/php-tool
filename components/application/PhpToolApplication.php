<?php

namespace app\components\application;
use yii\web\Application;

/**
 * Class PhpToolApplication
 * @package app\components\application
 * @property  \app\components\PhpToolComponent $phpTool
 */
class PhpToolApplication extends Application
{
    public function __construct($config = [])
    {
        parent::__construct($config);
    }
}