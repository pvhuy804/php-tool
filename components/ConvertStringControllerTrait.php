<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 10:55 PM
 */

namespace app\components;


use app\models\ConvertStringForm;
use app\services\ConstantService;
use app\services\ConvertStringService;
use yii\web\NotFoundHttpException;

trait ConvertStringControllerTrait
{
    public function actionIndex() {
        $programmingLanguages = ConstantService::getProgrammingLanguages();
        return $this->render('@app/views/convert-string/index', [
            'programmingLanguages' => $programmingLanguages,
            'options' => ConstantService::getFormatOptions(),
            'model' => ConvertStringService::getModelDefault()
        ]);
    }

    public function actionConvertString() {
        if(\Yii::$app->request->isPost) {
            $model = new ConvertStringForm();
            $model->load(\Yii::$app->request->post());
            $result = [
                'state' => 'fail',
                'message' => ''
            ];
            if($model) {
                $model->options = \Yii::$app->request->post()['ConvertStringForm']['options'];
                $strOut = ConvertStringService::getOutStr($model);
                $result['data'] = $strOut;
            }
            echo json_encode($result);
        } else {
            throw new NotFoundHttpException();
        }
    }
}