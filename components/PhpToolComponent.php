<?php

namespace app\components;

use app\models\User;

/**
 * Class PhpToolComponent
 * @package app\components
 */
class PhpToolComponent extends BaseComponent
{
    public $usernameAccess;

    public function checkExistUsernameUrl() {
        $model = User::findByUsername($this->usernameAccess);
        return !!$model;
    }

    public function createModuleUrl($function) {
        if(\Yii::$app->user->identity->role == User::ROLE_ADMIN) {
            return \Yii::$app->getHomeUrl() . "admin/$function";
        }
        return \Yii::$app->getHomeUrl() . \Yii::$app->user->identity->username."/$function";
    }
}