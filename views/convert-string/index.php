<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'Convert String';

?>
<style>
    .text-area {
        height: 85vh;
        resize: none;
        font-family: 'Courier New', Courier, monospace !important;
    }
</style>
<?php $form = ActiveForm::begin([
    'id' => 'form',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<?= $form->field($model, 'programmingLanguage')->dropdownList($programmingLanguages); ?>
<?= $form->field($model, 'options')->checkboxList($options)->label(false); ?>
<div class="block" style="margin-top: 10px">
    <div class="str-in col-sm-5">
        <?= $form->field($model, 'string')->textarea(['rows' => '6', 'class' => 'col-sm-12 text-area', 'id' => 'str-in'])->label(false) ?>
    </div>
    <div class="btn-convert col-sm-2" style="height: 85vh">
        <div>
            <button class="btn btn-default btn-cv-string" id="btn-cv-variable" style="width: 100%; margin-bottom: 5px"><?=Yii::t('main', 'to variable string') ?></button>
        </div>
    </div>
    <div class="str-out col-sm-5">
        <pre class="col-sm-12 text-area" style="height: 85vh; resize: none; font-family: 'Courier New', Courier, monospace">
            <p style="color: #cc0000" id="str-out"></p>
        </pre>
    </div>
</div>
<?php ActiveForm::end() ?>
<?php
$this->registerJsFile('@web/js/convert-string.js', ['depends' => \yii\web\YiiAsset::className()]);
?>
<script>
    var csrf = '<?=Yii::$app->request->getCsrfToken()?>';
    var getStrOutUrl = '<?=\yii\helpers\Url::toRoute('convert-string')?>';
</script>
