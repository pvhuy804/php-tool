<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

$title = 'Register';
?>
<div class="header-link">
    <a href="<?php echo Yii::$app->getHomeUrl()?>home">Home</a>
    <span>|</span>
    <a href="<?php echo Yii::$app->getHomeUrl()?>login">Login</a>
</div>
<div class="site-register" style="margin-top: 20px">
    <?php $form = ActiveForm::begin([
        'id' => 'register-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "<div class='col-sm-8'>{input}{error}</div>",
            'horizontalCssClasses' => [
                'error' => 'error-form',
            ],
        ],
    ]); ?>
    <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-field']) ?>
    <?= $form->field($model, 'email')->textInput(['class' => 'form-field']) ?>
    <?= $form->field($model, 'password')->passwordInput(['class' => 'form-field']) ?>
    <?= $form->field($model, 'passwordConfirm')->passwordInput(['class' => 'form-field']) ?>
    <div class="form-group" style="padding-left: 15px">
        <?= Html::submitButton('', ['name' => 'register-button']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
