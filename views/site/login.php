<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="header-link">
    <a href="<?php echo Yii::$app->getHomeUrl()?>home">Home</a>
    <span>|</span>
    <a href="<?php echo Yii::$app->getHomeUrl()?>register">Register</a>
</div>
<div class="site-login" style="margin-top: 20px">
    <?php
        $autoFocus = $model->username && !$model->password ? false : true;
        $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "<div class='col-sm-8'>{input}{error}</div>",
            'horizontalCssClasses' => [
                'error' => 'error-form',
            ],
        ],
    ]); ?>
        <?= $form->field($model, 'username')->textInput(['autofocus' => $autoFocus, 'class' => 'form-field']) ?>
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => !$autoFocus, 'class' => 'form-field']) ?>
        <div class="form-group" style="padding-left: 15px">
            <?= Html::submitButton('', ['name' => 'login-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
