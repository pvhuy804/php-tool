<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="header-link">
    <a href="<?php echo Yii::$app->getHomeUrl()?>home">Home</a>
</div>
<div class="site-error" style="margin-top: 20px">
    <div style="font-family: 'Courier New', Courier, monospace !important">
        <?= nl2br(Html::encode($message)) ?>
    </div>
</div>
