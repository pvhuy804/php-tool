<?php

namespace app\controllers;

use app\models\RegisterForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {
        $this->layout='home';
        if (parent::beforeAction($action)) {
            if ($action->id=='error')
                $this->layout ='home';
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            if(Yii::$app->getUser()->identity->role == User::ROLE_ADMIN) {
                return $this->redirect('admin');
            }
            return $this->redirect(Yii::$app->user->identity->username);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            $this->loginRedirect();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->loginRedirect();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function loginRedirect()
    {
        if (Yii::$app->getUser()->identity->role == User::ROLE_ADMIN) {
            return $this->redirect('admin');
        } else if (Yii::$app->getUser()->identity->role == User::ROLE_USER) {
            return $this->redirect(Yii::$app->getUser()->identity->username);
        }
        return $this->redirect(Yii::$app->getUser()->identity->username);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * @return Response|string
     */
    public function actionRegister() {
        $model = new RegisterForm();
        if($model->load(Yii::$app->request->post()) && $model->registerUser()) {
            $userModel = new LoginForm();
            $userModel->username = $model->username;
            return $this->render('login', [
                'model' => $userModel
            ]);
        }
        return $this->render('register', [
            'model' => $model
        ]);
    }
}
