/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 11:03 PM
 */
$(function () {
    $('.btn-cv-string').click(function (e) {
        e.preventDefault();
        $.ajax({
            url: getStrOutUrl,
            type: 'POST',
            data: $('#form').serializeArray(),
            success: function (result) {
                $('#str-out').html(jQuery.parseJSON(result).data);
            }
        });
    });
});