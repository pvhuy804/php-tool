<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 10:16 PM
 */
namespace app\services;

use Yii;

class ConstantService
{
    // for convert string
    const OPTION_TYPE_STR_TO_VAR = 'real string';
    const OPTION_TYPE_VAR_TO_STR = 'variable';

    const OPTION_FORMAT_REMOVE_EMPTY_LINE = 11;
    const OPTION_FORMAT_INLINE = 12;
    const OPTION_FORMAT_WORD_WRAP = 13;
    const OPTION_FORMAT_CODE_FORMATTER = 14;
    const OPTION_FORMAT_NOT_CODE_FORMATTER = 15;

    const OPTION_LANGUAGE_PHP = 21;
    const OPTION_LANGUAGE_JS = 22;

    const BREAK_LINE = "\r\n";


    public static function getProgrammingLanguages() {
        return [
            static::OPTION_LANGUAGE_PHP => Yii::t('main', 'php'),
            static::OPTION_LANGUAGE_JS => Yii::t('main', 'java script'),
        ];
    }

    public static function getFormatOptions() {
        return [
            static::OPTION_FORMAT_REMOVE_EMPTY_LINE => Yii::t('main', 'remove empty line'),
            static::OPTION_FORMAT_INLINE => Yii::t('main', 'inline'),
            static::OPTION_FORMAT_CODE_FORMATTER => Yii::t('main', 'code formatter'),
        ];
    }
}