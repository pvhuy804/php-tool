<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/18/2017
 * Time: 9:39 PM
 */

namespace app\services;


use app\models\ConvertStringForm;
use yii\base\Component;

class ConvertStringService extends Component
{
    public static function getModelDefault()
    {
        $model = new ConvertStringForm();
        $model->programmingLanguage = ConstantService::OPTION_LANGUAGE_PHP;
        $model->options = [];
        $model->string = '';
        return $model;
    }

    /**
     * @param $model ConvertStringForm
     * @return mixed
     */
    public static function getOutStr($model)
    {
        $options = $model->options;
        if(!is_array($options)) {
            $options = [];
        }

        $strInArr = explode(ConstantService::BREAK_LINE, $model->string);
        if($isInline = in_array(ConstantService::OPTION_FORMAT_INLINE, $options)) {
            $lineConcatenationString = ' ';
        } else {
            $stringConcatenationChar = static::getStringConcatenationChar($model->programmingLanguage);
            $lineConcatenationString = "\"\r\n$stringConcatenationChar \"";
            $isCodeFormatter = in_array(ConstantService::OPTION_FORMAT_CODE_FORMATTER, $options) ? true : false;
            if (in_array(ConstantService::OPTION_FORMAT_REMOVE_EMPTY_LINE, $options)) {
                $size = sizeof($strInArr);
                for ($index = 0; $index < $size; $index++) {
                    if (trim($strInArr[$index]) == '') {
                        unset($strInArr[$index]);
                    }
                }
            }
        }
        $strOut = implode($lineConcatenationString, $strInArr);
        return '"' . $strOut . '"';
    }

    private static function getStringConcatenationChar($language)
    {
        $char = '+';
        switch ($language) {
            case ConstantService::OPTION_LANGUAGE_PHP:
                $char = '.';
                break;
            case ConstantService::OPTION_LANGUAGE_JS:
                break;
            default:
                break;
        }
        return $char;
    }
}