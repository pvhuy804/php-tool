<?php

use yii\db\Migration;

class m170916_012959_profile extends Migration
{
    public function safeUp()
    {
        $this->createTable('profile', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer()->notNull()->unique(),
            'name' => $this->string()->notNull(),
            'gender' => $this->integer()->notNull(),
            'birthday' => $this->string()->notNull(),
        ]);

        $this->insert('profile', [
            'userId' => 1,
            'name' => 'admin',
            'gender' => 0,
            'birthday' => '01/01/1990'
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('profile');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170916_012959_profile cannot be reverted.\n";

        return false;
    }
    */
}
