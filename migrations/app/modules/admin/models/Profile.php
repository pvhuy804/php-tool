<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 9:42 PM
 */

namespace app\modules\admin\models;

use yii\db\ActiveRecord;

/**
 * Class Profile
 * @package app\modules\admin\models
 * @property int id
 * @property string userId
 * @property string name
 * @property string gender
 * @property string birthday
 */
class Profile extends ActiveRecord
{
    const GENDER_UNKNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public function rules()
    {
        return [
            [['userId', 'name', 'gender', 'birthday'], 'required'],
            ['birthday', 'date']
        ];
    }
}