<?php

use yii\db\Migration;
use app\models\User;

class m170914_013202_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('user', [
            'id'=> $this->primaryKey(),
            'username'=> $this->string()->notNull()->unique(),
            'email'=> $this->string()->notNull()->unique(),
            'password'=>  $this->string()->notNull(),
            'role'=>  $this->integer()->notNull()
        ]);
        $this->insert('user', [
            'username'=>'admin',
            'email'=>'admin@example.com',
            'password'=> '$2y$13$Lr0R9F4RdFXs.Q65ugQhOuq965CNmqDd6leSuVCoDUBKqUv/Qvr/m', // 'admin'
            'role'=> 1,
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170914_013202_user cannot be reverted.\n";

        return false;
    }
    */
}
