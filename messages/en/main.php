<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 10:20 PM
 */

return [
    'php' => 'PHP',
    'java script' => 'Java Script',
    'convert string' => 'Convert String',
    'crypt' => 'Crypt',
    'profile' => 'Profile',
    'user' => 'User',
    'decrypt' => 'Decrypt',
    'to variable string' => 'To Variable String',
    'to real string' => 'To Real String',
];