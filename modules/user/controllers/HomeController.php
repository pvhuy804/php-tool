<?php

/**
 * Created by Cominit.
 * User: pham_vanhuy
 * Date: 9/19/2017
 * Time: 9:55 AM
 */
namespace app\modules\user\controllers;

use app\models\User;
use app\modules\user\components\BaseUserController;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class HomeController extends BaseUserController
{
    public function behaviors()
    {
        $rules = parent::behaviors()['access']['rules'];
        if(!Yii::$app->user->isGuest && Yii::$app->phpTool->checkExistUsernameUrl()
            && Yii::$app->user->identity->username != Yii::$app->phpTool->usernameAccess) {
            $rules = [
                [
                    'actions' => ['index'],
                    'allow' => true,
                    'roles' => ['@']
                ]
            ];
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
                'denyCallback'  => function ($rule, $action) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'Page nots found.'));
                },
            ],
        ];
    }

    public function actionIndex() {
        return $this->render('index');
    }
}