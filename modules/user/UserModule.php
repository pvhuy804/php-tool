<?php
namespace app\modules\user;

use yii\base\Module;

class UserModule extends Module
{
    public function init()
    {
        $this->layout = 'user';
        $this->defaultRoute = 'home';
        \Yii::$app->phpTool->usernameAccess = \Yii::$app->request->getQueryParam('username');
        parent::init();
    }
}