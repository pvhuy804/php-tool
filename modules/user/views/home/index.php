<?php
use app\models\User;

if(Yii::$app->phpTool->usernameAccess && Yii::$app->phpTool->checkExistUsernameUrl()) {
    $user = User::findByUsername(Yii::$app->phpTool->usernameAccess);
    echo $user->email;
}