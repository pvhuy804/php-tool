<?php
$this->title = 'Profile';
?>
<style>
    div.profile-form {
        margin: 0 0 0 15px;
    }
</style>
<div class="profile-form col-sm-4">
    <?= $this->render('_form', [
        'model' => $model,
        'genderList' => $genderList
    ]) ?>
</div>