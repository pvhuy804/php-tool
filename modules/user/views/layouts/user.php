<?php
use yii\helpers\Html;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="icon" href="<?php echo Yii::$app->getHomeUrl()."images/icon-ghost-png-3.png"?>" type="image/png" sizes="16x16">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div id="container">
    <div class="navbar navbar-inverse navbar-fixed-left">
        <div class="top-nav">
            <nav class="navbar navbar-default" style="border-radius:0;">
                <div class="container-fluid">
                    <div class="navbar-header col-sm-12">
                        <a class="navbar-brand" href="<?php echo Yii::$app->phpTool->createModuleUrl('home')?>" style="width: 100%; padding: 3px 0 3px 0;">
                            <img alt="Lwf" src="<?php echo Yii::$app->getHomeUrl()."images/icon-ghost-png-3.png"?>" height="44px" style="float: left;">
                            <span class="span-logo">Lwf</span>
                        </a>
                    </div>
                </div>
            </nav>
        </div>
        <ul class="admin-sidebar list-group">
            <li class="list-group-item"><a href="<?php echo Yii::$app->phpTool->createModuleUrl('crypt') ?>">Crypt</a></li>
            <li class="list-group-item"><a href="<?php echo Yii::$app->phpTool->createModuleUrl('profile') ?>">Profile</a></li>
            <li class="list-group-item"><a href="<?php echo Yii::$app->phpTool->createModuleUrl('user') ?>">User</a></li>
        </ul>
    </div>
    <div class="container content" style="margin: 0; width: 100%">
        <div class="row row-content">
            <?php foreach (Yii::$app->session->getAllFlashes() as $key => $value) {?>
                <div class="session-flash" type-alert="<?php echo $key ?>" style="position: fixed; z-index: 99; display: none;">
                    <?php echo "<p>$value</p>";?>
                </div>
            <?php } ?>
            <nav class="navbar navbar-default navbar-fixed-top" style="left: 15%">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div id="navbar" class="nav-bar-content navbar-collapse collapse" aria-expanded="false">
                        <ul class="nav navbar-nav content-navbar-left">
                        </ul>
                        <ul class="nav navbar-nav navbar-right" style="margin-right: auto">
                            <?php
                            echo Html::beginForm(['/site/logout'], 'post', ['class' => 'navbar-form navbar-right'])
                                . Html::submitButton(
                                    'Logout',
                                    ['class' => 'btn btn-a']
                                )
                                . Html::endForm();
                            ?>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="main-container" style="margin-top: 60px">
                <?= $content ?>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
<?php
$this->registerJs("
        if($('.session-flash').length) {
            var typeAlert = $('.session-flash').attr('type-alert');
            var color = '#000';
            switch(typeAlert) {
                case 'error':
                    color = '#FF0000';
                    break;
                case 'success':
                    color = '#008000';
                    break;
                default:
                    break;
            }
            $('.session-flash').css({'border': 'solid 1px ' + color, 'display': 'block'});
            $('.session-flash p').css('color', color);
        }
        $(function() {
            if($('.session-flash').length) {
                $('.session-flash').fadeOut(3000);
            }
        });
    ")
?>
</html>
<?php $this->endPage() ?>
