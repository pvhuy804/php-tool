<?php

/**
 * Created by Cominit.
 * User: pham_vanhuy
 * Date: 9/19/2017
 * Time: 9:54 AM
 */
namespace app\modules\user\components;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BaseUserController extends Controller
{
    public function behaviors()
    {
        $rules = [
            [
                'allow' => false,
                'roles' => ['@']
            ]
        ];
        if(!Yii::$app->user->isGuest) {
            $userRole = \Yii::$app->user->identity->role;
            if ($userRole == User::ROLE_USER && Yii::$app->user->identity->username == Yii::$app->phpTool->usernameAccess) {
                $rules[0]['allow'] = true;
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
                'denyCallback'  => function ($rule, $action) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'Page not found.'));
                },
            ],
        ];
    }
}