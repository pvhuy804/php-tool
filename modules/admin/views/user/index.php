<?php $this->registerCssFile('@web/css/table.css', ['depends' => 'app\assets\AdminAsset']);?>
<style>
    #user-table {
        margin: 30px 10px auto 10px;
    }
    .summary {
        float: right;
    }

    .user-type {
        background-color: #f8f8f8;
        border: none;
    }

    div.user-type-parent {
        line-height:4em;
    }

    .user-table__no-column {
        width: 5%;
        text-align: center;
    }

    .user-table__action-column {
        width: 10%;
        text-align: center;
    }

    table.user-table__table {
        width: 100%;
    }

</style>
<?php \yii\widgets\Pjax::begin(['id' => 'pjax-user-table']); ?>
<?= \yii\grid\GridView::widget([
    'id' => 'user-table',
    'dataProvider' => $dataProvider,
    'summary' => "<div class='summary'>Showing {begin} - {end} of {totalCount} items</div>",
    'tableOptions' => ['class' => 'user-table__table'],
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'header' => 'No',
            'headerOptions' => ['class' => 'user-table__no-column'],
            'contentOptions' => ['class' => 'user-table__no-column']
        ],
        'username',
        'email',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Action',
            'headerOptions' => ['class' => 'user-table__action-column'],
            'contentOptions' => ['class' => 'user-table__action-column']
        ],
    ]
]);?>
<?php \yii\widgets\Pjax::end(); ?>
<?php
    $this->registerJs("
        $(function() {
            $('.content-navbar-left').append(
                '<div class=\"user-type-parent\"><select class=\"user-type\">' +
                '<option value=\"\">All</option>' +
                '<option value=\"1\">Admin</option>' +
                '<option value=\"2\">User</option>' +
            '</select></div>'
            );
            $('.user-type').change(function () {
            $.pjax.reload({method: 'POST', container:'#pjax-user-table', timeout: 5000, data: {role: $(this).val()}});
        });
        });
    ");
?>
