<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $this->registerCssFile('@web/css/form.css', ['depends' => 'app\assets\AdminAsset']);?>
<?php $this->registerCssFile('@web/css/bootstrap-datepicker3.min.css', ['depends' => 'app\assets\AdminAsset']);?>
<?php $this->registerJsFile('@web/js/bootstrap-datepicker.min.js', ['depends' => 'yii\web\YiiAsset']);?>
<style>
    input.form-input-text {
        width: 100%;
    }

    form#profile-form label {
        font-weight: 600;
    }

    div#profile-gender label {
        float: left;
        width: 33%;
        font-weight: lighter;
    }
</style>
<?php
$form = ActiveForm::begin([
    'id' => 'profile-form',
    'enableClientValidation' => false,
    'enableAjaxValidation' => true,
    'fieldConfig' => [
        'template' => "{label}{input}{error}",
        'labelOptions' => [ 'class' => 'form-input-label' ]
    ]
]);
?>
    <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'disabled' => true]) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-input-text']) ?>
    <?= $form->field($model, 'gender')->radioList($genderList) ?>
    <?= $form->field($model, 'birthday')->textInput(['maxlength' => true, 'class' => 'form-input-text input-birthday']) ?>
    <?= $form->field($model, 'oldPassword')->passwordInput(['class' => 'form-input-text']) ?>
    <?= $form->field($model, 'newPassword')->passwordInput(['class' => 'form-input-text']) ?>
    <?= $form->field($model, 'newPasswordConfirm')->passwordInput(['class' => 'form-input-text']) ?>
    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-default', 'style' => 'font-weight: bold']) ?>
    </div>
<?php ActiveForm::end()?>
<?php
    $this->registerJs('
        $(".input-birthday").datepicker({format: "dd/mm/yyyy"});
    ');
?>
