<?php
$this->title = 'Encrypt';
?>
<div class="crypt-content" style="margin-top: 10px">
    <div class="str-in col-sm-5">
        <textarea id="str-in" class="col-sm-12" style="height: 85vh; resize: none; font-family: 'Courier New', Courier, monospace"></textarea>
    </div>
    <div class="btn-convert col-sm-2" style="height: 85vh">
        <div>
            <button class="btn btn-default btn-crypt" id="btn-crypt" style="width: 100%; margin-bottom: 5px"><?=Yii::t('main', 'crypt')?></button>
            <button class="btn btn-default btn-crypt" id="btn-decrypt" style="width: 100%; margin-top: 5px"><?=Yii::t('main', 'decrypt')?></button>
        </div>
    </div>
    <div class="str-out col-sm-5">
        <textarea id="str-out" class="col-sm-12" style="height: 85vh; resize: none; font-family: 'Courier New', Courier, monospace"></textarea>
    </div>
</div>

<script>
    $(function () {
        $('.btn-crypt').click(function () {
            var type = $(this).attr('id') === 'btn-crypt'? 'encrypt' : 'decrypt';
            $.ajax({
                url: 'http://localhost/php-tool/web/admin/crypt/crypt',
                type: 'POST',
                data: {
                    data: {
                        strIn: $('#str-in').val(),
                        type: type
                    },
                    _csrf: '<?=Yii::$app->request->getCsrfToken()?>'
                },
                success : function (result) {
                    $('#str-out').val(jQuery.parseJSON(result).strOut);
                }
            });
        });
    });
</script>