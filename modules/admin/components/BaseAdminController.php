<?php

namespace app\modules\admin\components;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BaseAdminController extends Controller
{
    public function behaviors()
    {
        $rules = [
            [
                'allow' => false,
                'roles' => ['@']
            ]
        ];
        if(!Yii::$app->user->isGuest) {
            $userRole = \Yii::$app->user->identity->role;
            if ($userRole == User::ROLE_ADMIN) {
                $rules[0]['allow'] = true;
            }
        }
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => $rules,
                'denyCallback'  => function ($rule, $action) {
                    throw new ForbiddenHttpException(Yii::t('yii', 'Page not found.'));
                },
            ],
        ];
    }
}