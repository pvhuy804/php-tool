<?php

namespace app\modules\admin\models;

use app\models\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class UserSearch extends User
{
    public function rules()
    {
        return [
            ['role', 'integer'],
            [['username', 'email', 'role'], 'safe']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($param) {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->role = isset($param['role']) ? $param['role'] : '';
        if(!$this->validateRole()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'role' => $this->role
        ]);

        return $dataProvider;
    }

    public function validateRole() {
        return is_numeric($this->role);
    }
}