<?php
/**
 * Created by Cominit.
 * User: pham_vanhuy
 * Date: 9/14/2017
 * Time: 4:36 PM
 */

namespace app\modules\admin\controllers;
use app\modules\admin\components\BaseAdminController;


class CryptController extends BaseAdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCrypt()
    {
        $result = [
            'strOut' => '',
            'state' => false
        ];
        if(\yii::$app->request->isAjax) {
            $data = \yii::$app->request->post('data');
            $strIn = isset($data['strIn']) ? $data['strIn'] : '';
            $type = isset($data['type']) ? $data['type'] : '';
            if($strIn != '' && $type != '') {
                $result['strOut'] = $this->dec_enc($type, $strIn);
                $result['state'] = true;
            }
        }
        return json_encode($result);
    }

    function dec_enc($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'MGJBdkdJOW5MN2dKUi9RbURYZTZmQT09';
        $secret_iv = 'MGJBdkdJOW5MN2dKUi9RbURYZTZmQT09';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        }
        else if( $action == 'decrypt' ){
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }
}