<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 10:14 PM
 */

namespace app\modules\admin\controllers;


use app\components\ConvertStringControllerTrait;
use app\modules\admin\components\BaseAdminController;

class ConvertStringController extends BaseAdminController
{
    use ConvertStringControllerTrait;
}