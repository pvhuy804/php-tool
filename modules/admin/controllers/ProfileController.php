<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 9/15/2017
 * Time: 6:47 PM
 */

namespace app\modules\admin\controllers;

use app\models\User;
use app\modules\admin\components\BaseAdminController;
use app\models\Profile;
use yii\web\Response;
use yii\widgets\ActiveForm;

class ProfileController extends BaseAdminController
{
    public function actionIndex() {
        $userId = \Yii::$app->user->getId();
        $model = Profile::getProfileByUserId($userId);
        if (!$model) {
            $model = new Profile();
            $model->userId = $userId;
        }
        if($model->load(\Yii::$app->request->post())) {
            if ($model->oldPassword || $model->newPassword) {
                $model->setScenario('changePassword');
            }
            if(\Yii::$app->request->isAjax) {
                \Yii::$app->response->format = Response::FORMAT_JSON;
                return ActiveForm::validate($model);
            } else {
                if ($model->save()) {
                    if($model->oldPassword) {
                        $user = User::findOne($userId);
                        $user->password = User::generateHashPassword($model->newPassword);
                        $user->save();
                    }
                    \Yii::$app->session->setFlash('success', 'Save profile success!');
                    $model->oldPassword = $model->newPassword = $model->newPasswordConfirm = '';
                } else {
                    \Yii::$app->session->setFlash('error', 'Save profile failed!');
                }
            }
        }
        $model->username = \Yii::$app->user->identity->username;
        $model->email = \Yii::$app->user->identity->email;
        return $this->render('index', [
            'model' => $model,
            'genderList' => Profile::GENDER_LIST
        ]);
    }
}