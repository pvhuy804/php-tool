<?php

namespace app\modules\admin\controllers;

use app\modules\admin\components\BaseAdminController;

/**
 * Default controller for the `admin` module
 */
class HomeController extends BaseAdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
