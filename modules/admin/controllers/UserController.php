<?php
/**
 * Created by Cominit.
 * User: pham_vanhuy
 * Date: 9/15/2017
 * Time: 10:29 AM
 */

namespace app\modules\admin\controllers;


use app\modules\admin\models\UserSearch;
use app\modules\admin\components\BaseAdminController;

class UserController extends BaseAdminController
{
    public function actionIndex() {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->post());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}