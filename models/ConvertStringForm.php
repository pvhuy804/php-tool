<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 12/14/2017
 * Time: 10:53 PM
 */

namespace app\models;


use yii\base\Model;

class ConvertStringForm extends Model
{
    public $programmingLanguage;
    public $options;
    public $string;

    public function rules()
    {
        return [
            [['programmingLanguage', '$options', 'string'], 'safe']
        ];
    }
}