<?php
namespace app\models;
use yii\base\Model;

class RegisterForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $passwordConfirm;
    public $role = 2;

    public function rules() {
        return [
            [['username', 'email', 'password', 'passwordConfirm'], 'required'],
            ['username', 'string', 'min' => 4],
            ['username', 'validateUsername'],
            ['email', 'email'],
            ['email', 'validateEmail'],
            [['password', 'passwordConfirm'], 'string', 'min' => 6],
            ['passwordConfirm', 'compare', 'compareAttribute' => 'password', 'message' => 'Password Confirm must be equal to Password.'],
        ];
    }

    public function validateUsername($attribute, $param) {
        if(!$this->hasErrors()) {
            $user = User::findByUsername($this->username);
            if($user) {
                $this->addError($attribute, 'Username is existed');
            }
        }
    }

    public function validateEmail($attribute, $param) {
        if(!$this->hasErrors()) {
            $user = User::findByEmail($this->email);
            if($user) {
                $this->addError($attribute, 'Email is existed');
            }
        }
    }

    public function registerUser() {
        if($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->password = User::generateHashPassword($this->password);
            $user->role = $this->role;
            if($user->save()) {
                return true;
            }
        }
        return false;
    }
}