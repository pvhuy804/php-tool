<?php
/**
 * Created by pvhuy84@gmail.com
 * Date: 9/15/2017
 * Time: 11:33 PM
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * Class Profile
 * @package app\modules\admin\models
 * @property integer $id
 * @property integer $userId
 * @property string $name
 * @property integer $gender
 * @property string $birthday
 */
class Profile extends ActiveRecord
{

    public $username;
    public $email;
    public $oldPassword;
    public $newPassword;
    public $newPasswordConfirm;

    const GENDER_UNKNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_LIST = [
        self::GENDER_UNKNOWN => 'Unknown',
        self::GENDER_MALE => 'Male',
        self::GENDER_FEMALE => 'Female'
    ];

    public static function tableName()
    {
        return 'profile';
    }

    public function rules()
    {
        return [
            [['userId', 'name', 'gender', 'birthday'], 'required'],
            [['userId', 'gender'], 'integer'],
            ['birthday', 'validateBirthday'],
            [[ 'oldPassword', 'newPassword', 'newPasswordConfirm'], 'safe'],
            [[ 'oldPassword', 'newPassword', 'newPasswordConfirm'], 'required', 'on' => 'changePassword'],
            ['oldPassword', 'validateOldPassword'],
            [['newPassword', 'newPasswordConfirm'], 'string', 'min' => 6],
            ['newPasswordConfirm', 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    public static function getProfileByUserId($userId) {
        return static::findOne(['userId' => $userId]);
    }

    public function validateOldPassword($attribute, $param = null) {
        if(!$this->hasErrors()) {
            $user = User::findOne(Yii::$app->user->getId());
            if(!Yii::$app->security->validatePassword($this->oldPassword, $user->password)) {
                $this->addError($attribute, 'Old Password invalid');
            }
        }
    }

    public function validateBirthday($attribute, $param) {
        if(!$this->hasErrors()) {
            if (!preg_match("/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/", $this->birthday)) {
                return $this->addError($attribute, 'Birthday invalid format');
            }
        }
    }
}