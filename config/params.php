<?php

return [
    'adminEmail' => 'admin@example.com',
    'dateFormat' => 'Y-m-d',
];
